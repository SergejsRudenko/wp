<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.$>j2{1l3,_?TWdk)bN*pIwwndrg-8+9W:sJBRM De N*]68J9)jxZbBe*jeyn6Y' );
define( 'SECURE_AUTH_KEY',  'ird#$arZrA} MD9GY6vAM~:Yhz:<*hN U.m({fBz8$ ]0<p.]DfJpX?ks< [fV9v' );
define( 'LOGGED_IN_KEY',    '>7T_.I:gL+Zl=qXIK}4N~IRT40?8,Ut%e*cyHFwmog/b<EM2;Q<2ND;MMvlEGRv&' );
define( 'NONCE_KEY',        ' f$zBE Rf8.dI]O4|I/*3L)KW-?qtR)rUS%x5qK6;CLnE2$ZKFNfYP51FvPi+>B$' );
define( 'AUTH_SALT',        '=J1iDnAvvZ2kWlKi)LNIt0#&w~/gnS^d:&7?7D!7dXdD3zK:egha !^YcAlWtF}#' );
define( 'SECURE_AUTH_SALT', ',f^ziWoijL. gx+|U Ezb7?B2l~T#)*?StRhWhu@:1fK%v&Faq|CcBt)~v$<wc1=' );
define( 'LOGGED_IN_SALT',   '?W7%t]8XoSRw)8<3pjwv8CQD99HiCbk7=C?7=er0c1JZP[@Ib?2hdwGBp?C&5,P9' );
define( 'NONCE_SALT',       't5fa,jTl1x51 XK)JX.7h{tTU5@T3F~]J!:YP+nB-cIBg{aHu16BhUxlfs+V,FcV' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
